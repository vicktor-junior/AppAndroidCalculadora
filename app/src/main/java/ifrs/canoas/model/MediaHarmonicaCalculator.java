package ifrs.canoas.model;

import java.util.ArrayList;

/**
 * Created by marcio on 06/08/17.
 */

public class MediaHarmonicaCalculator {
    private ArrayList<ItemMedia> notas = new ArrayList();

    public void addNota(String label, double nota, double peso) throws Exception{

            notas.add(new ItemMedia(label,nota,peso));

    }

    public String listaNotas() {
       String notasString = "";
        for (ItemMedia notaItem: notas) {
            notasString += notaItem.getNota()+"\n";
        }
        return notasString;
    }

    public int listaTamanho() {
        return notas.size();
    }


    /**
     *
     *
     * @return
     * @throws Exception
     */
    public double calculaNota() throws Exception {
        double media = 0;
        double pesos = 0;
        double division =0;
        if(notas.size()>1){
            for (ItemMedia notaItem: notas) {
                if (notaItem.getNota() <= 0) {
                    notaItem.setNota(0.0001);
                }
                pesos += notaItem.getPeso();
                division += notaItem.getPeso()/notaItem.getNota();
            }
            if (pesos == 10) {
                media = pesos / division;
            } else {
                throw new Exception("Peso inválido (somatório diferente de 10)");
            }

        }else{
            throw new Exception("Quantidade de notas insuficientes");
        }
        return media;
    }
}
