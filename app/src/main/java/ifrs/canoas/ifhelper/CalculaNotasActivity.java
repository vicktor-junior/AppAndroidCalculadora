package ifrs.canoas.ifhelper;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import ifrs.canoas.model.MediaHarmonicaCalculator;

/**
 * Created by Aluno on 10/08/2017.
 */

public class CalculaNotasActivity extends Activity {
    private MediaHarmonicaCalculator calculator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcula_notas);
        calculator = new MediaHarmonicaCalculator();
    }

    public void addNotas(View v) {
        EditText label = (EditText) findViewById(R.id.txtLabel);
        EditText valor = (EditText) findViewById(R.id.txtValor);
        EditText peso = (EditText) findViewById(R.id.txtPeso);
    try {

            calculator.addNota(label.getText().toString(), //converte a label
                    Double.parseDouble(valor.getText().toString()), //converte o valor
                    Double.parseDouble(peso.getText().toString())); // converte o peso

        TextView txtNotas = (TextView) findViewById(R.id.txtNotas);
        txtNotas.setText(calculator.listaNotas());
    } catch (Exception e) {
        e.printStackTrace();
    }



    }

    public void calcula(View v) throws Exception {
        if(calculator.listaTamanho() >= 2) {
            TextView txtMedia = (TextView) findViewById(R.id.txtMedia);
            txtMedia.setText(Double.toString(calculator.calculaNota()));
        }

    }




}
